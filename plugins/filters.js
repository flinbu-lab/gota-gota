import Vue from "vue"
import Numeral from "numeral"

const formatCurrencyFull = (amount, symbol = "$") => {
  let num = Numeral(amount)
  return `${symbol}${num.format("0,0", Math.ceil)}`
}

const formatCurrencyDetailed = amount => {
  let num = Numeral(amount)
  return num.format("0,0[.]00", Math.floor)
}

const formatNumber = amount => {
    let num = Numeral(amount)
    return num.format("0,0", Math.floor)
}
  
Vue.filter("upperCase", value => {
  if (!value) return ""
  return `${value}`.toUpperCase()
})
Vue.filter("lowerCase", value => {
  if (!value) return ""
  return `${value}`.toLowerCase()
})

Vue.filter("capitalize", value => {
  if (!value) return ""
  value = value.toString()
  value.toLowerCase()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('number', amount => formatNumber(amount))

Vue.filter("currency", (amount, symbol = "$") =>
  formatCurrencyFull(amount, symbol)
)

Vue.filter("currencyDetailed", amount => formatCurrencyDetailed(amount))

Vue.filter("phoneNumber", phone => {
  if (!phone) return
  phone = phone.toString()
  let last = phone.slice(-4)
  let middle = phone.slice(-7).slice(0, -4)
  let first = phone.slice(-10).slice(0, -7)
  let code = phone.slice(0, -10)

  return `${code} (${first}) ${middle}-${last}`
})

Vue.filter("bigNumber", (value, max, sup) => {
  if (!value) return
  if (!max) max = 9
  value = Number(value)
  let formatted = value
  if (value > max) {
    let plus = "+"
    if (sup) {
      plus = "<sup>+</sup>"
    }
    formatted = max.toString() + plus
  }

  return formatted
})

Vue.filter("protectPhoneNumber", (phone, showNumbers = 4) => {
  if (!phone) return ""
  phone = phone.toString()
  const lastNumbers = phone.substr(phone.length - showNumbers)
  let formatted = ""
  for (let i = 0; i < phone.length - showNumbers; i++) {
    formatted = "*"
  }
  return `${formatted}${lastNumbers}`
})
